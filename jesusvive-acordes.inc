\context ChordNames \chords {
	\set majorSevenSymbol = \markup { "maj7" }
	\set chordChanges = ##t

	% intro
	d1 d1 d1 d1

	% en la arena...
	d1 a1 b1:m fis1:m
	g1 d1 a1 a1
	d1 a1 b1:m fis1:m
	g1 d1
	% 4/4
	e2:m a2 d1 d1:7

	% siente su presencia...
	g1 d1 g1 d1
	g1 d2 b2:m e2:m a2
	d1 d1:7

	% pide y el te dara...
	g1 d1 g1 d1
	g1 d4. b8:m ~ b2:m e2:m a2
	% 2/2
	d1

	% glue
	   d1 d1 d1

	% resucita con el y cree...
	d1 a1 b1:m fis1:m
	g1 d1 a1 a1
	d1 a1 b1:m fis1:m
	g1 d1
	% 4/4
	e2:m a2 d1 d1:7

	% siente su presencia...
	g1 d1 g1 d1
	g1 d2 b2:m e2:m a2
	d1 d1:7

	% pide y el te dara...
	g1 d1 g1 d1
	g1 d4. b8:m ~ b2:m e2:m a2
	% 2/2
	d1

	% glue
	   d1 d1 d1

	% en el mar de tus pensamientos...
	d1 a1 b1:m fis1:m
	g1 d1 a1 a1
	d1 a1 b1:m fis1:m
	g1 d1
	% 4/4
	e2:m a2 d1 d1:7

	% siente su presencia...
	g1 d1 g1 d1
	g1 d2 b2:m e2:m a2
	d1 d1:7

	% pide y el te dara...
	g1 d1 g1 d1
	g1 d4. b8:m ~ b2:m e2:m a2
	d1 e2:m a2
	d1 e2:m a2
	d1 d1
}
