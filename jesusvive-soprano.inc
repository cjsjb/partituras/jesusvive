\context Staff = "soprano" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Soprano"
	\set Staff.shortInstrumentName = "S."
	\set Staff.midiInstrument = "Voice Oohs"
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-soprano" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\clef "treble"
		\key d \major

		R1*4  |
%% 5
		r4 a' 8 a' a' a' 4 a' 8  |
		a' 4 g' 8 fis' 4 e' 4.  |
		r4 fis' 8 fis' fis' fis' 4 g' 8  |
		fis' 4 e' 8 d' 4 cis' 4.  |
		r4 b 8 b d' d' e' d' ~  |
%% 10
		d' 8 a ~ a 2 r8 a  |
		fis' 8 e' d' e' ~ e' 2 ~  |
		e' 2. r4  |
		r4 a' 8 a' a' a' 4 a' 8  |
		a' 4 g' 8 fis' 4 e' 4.  |
%% 15
		r4 fis' 8 fis' fis' fis' 4 g' 8  |
		fis' 4 e' 8 d' 4 cis' 4.  |
		r4 g' g' 4. g' 8  |
		g' 8 fis' ~ fis' 2 r8 a  |
		fis' 8 e' d' e' 4. d' 4  |
%% 20
		\time 4/4
		d' 1 ~  |
		d' 2 r  |
		b 4 d' d' 8 e' 4 fis' 8 ~  |
		fis' 8 a 2..  |
		r4 b 8 b d' e' 4 fis' 8 ~  |
%% 25
		fis' 1  |
		r4 b 8 b d' e' 4 fis' 8 ~  |
		fis' 8 a 4 r8 fis' 4 fis'  |
		g' 4. e' 8 d' cis' 4 d' 8 ~  |
		d' 1 ~  |
%% 30
		d' 2 r  |
		r4 g' g' 4. a' 8  |
		fis' 8 e' d' d' ~ d' 2  |
		r4 g' 8 g' 4 g' a' 8  |
		a' 1  |
%% 35
		r4 g' 8 g' g' 4. a' 8  |
		fis' 4 ( e' 8 ) d' 4 r8 b b  |
		fis' 4. e' 8 d' cis' 4 d' 8 ~  |
		\time 2/2
		d' 1  |
		R1*3  |
		r4 a' 8 a' a' a' 4 a' 8  |
		a' 4 g' 8 fis' 4 e' 4.  |
		r4 fis' 8 fis' fis' fis' 4 g' 8  |
%% 45
		fis' 4 e' 8 d' 4 cis' 4.  |
		r4 b 8 b d' d' e' d' ~  |
		d' 8 a ~ a 4. r8 a a  |
		fis' 8 e' d' e' 4. fis' 8 e' ~  |
		e' 2. r4  |
%% 50
		r4 a' 8 a' a' a' 4 a' 8  |
		a' 4 g' 8 fis' 4 e' 4.  |
		r4 fis' 8 fis' fis' fis' 4 g' 8  |
		fis' 4 e' 8 d' 4 cis' 4.  |
		r4 b 8 b d' d' e' d' ~  |
%% 55
		d' 8 a 2 r8 a a  |
		fis' 4 e' e' d'  |
		\time 4/4
		d' 1 ~  |
		d' 2 r  |
		b 4 d' d' 8 e' 4 fis' 8 ~  |
%% 60
		fis' 8 a 2..  |
		r4 b 8 b d' e' 4 fis' 8 ~  |
		fis' 1  |
		r4 b 8 b d' e' 4 fis' 8 ~  |
		fis' 8 a 4 r8 fis' 4 fis'  |
%% 65
		g' 4. e' 8 d' cis' 4 d' 8 ~  |
		d' 1 ~  |
		d' 2 r  |
		r4 g' g' 4. a' 8  |
		fis' 8 e' d' d' ~ d' 2  |
%% 70
		r4 g' 8 g' 4 g' a' 8  |
		a' 1  |
		r4 g' 8 g' g' 4. a' 8  |
		fis' 4 ( e' 8 ) d' 4 r8 b b  |
		fis' 4. e' 8 d' cis' 4 d' 8 ~  |
%% 75
		\time 2/2
		d' 1  |
		R1*3  |
		r4 a' 8 a' a' a' 4 a' 8  |
%% 80
		a' 4 g' 8 fis' 4 e' 4.  |
		r4 fis' 8 fis' fis' fis' 4 g' 8  |
		fis' 4 e' 8 d' 4 cis' 4.  |
		r4 g' g' 4. g' 8  |
		g' 8 fis' ~ fis' 2 r8 a  |
%% 85
		fis' 8 e' d' e' 4. fis' 8 e' ~  |
		e' 2. r4  |
		r4 a' 8 a' a' a' 4 a' 8  |
		a' 4 g' 8 fis' 4 e' 4.  |
		r4 fis' 8 fis' fis' fis' 4 g' 8  |
%% 90
		fis' 4 e' 8 d' 4 cis' 4.  |
		r4 g' g' 4. g' 8  |
		g' 8 fis' ~ fis' 2 r8 a  |
		fis' 8 e' d' e' 4. d' 4  |
		\time 4/4
		d' 1 ~  |
%% 95
		d' 2 r  |
		b 4 d' d' 8 e' 4 fis' 8 ~  |
		fis' 8 a 2..  |
		r4 b 8 b d' e' 4 fis' 8 ~  |
		fis' 1  |
%% 100
		r4 b 8 b d' e' 4 fis' 8 ~  |
		fis' 8 a 4 r8 fis' 4 fis'  |
		g' 4. e' 8 d' cis' 4 d' 8 ~  |
		d' 1 ~  |
		d' 2 r  |
%% 105
		r4 g' g' 4. a' 8  |
		fis' 8 e' d' d' ~ d' 2  |
		r4 g' 8 g' 4 g' a' 8  |
		a' 1  |
		r4 g' 8 g' g' 4. a' 8  |
%% 110
		fis' 4 ( e' 8 ) d' 4 r8 b b  |
		fis' 4. e' 8 d' cis' 4 d' 8 ~  |
		d' 2 r4 b 8 b  |
		fis' 4. e' 8 d' cis' 4 d' 8 ~  |
		d' 2 r4 b 8 b  |
%% 115
		fis' 4. e' 8 d' cis' 4 d' 8 ~  |
		d' 1  |
		R1*2  |
		\bar "|."
	}

	\new Lyrics \lyricsto "voz-soprano" {
		En "la a" -- re -- "na en" -- cuen -- tras sus hue -- llas,
		y su ros -- "tro en" las co -- sas be -- llas,
		en la ri -- sa "y el" llan -- to, "te en" -- cuen -- tras con Él. __

		Ca -- mi -- nan -- do con Él ca -- mi -- nas,
		en las ro -- sas "y en" las es -- pi -- nas,
		bus -- ca su ros -- tro, re -- cuer -- da que vi -- "vo es" -- tá. __

		Sien -- te su pre -- sen -- cia
		y a -- dó -- ra -- lo, __
		a -- la -- ba su nom -- bre,
		da -- le glo -- rias al Se -- ñor. __

		Pi -- de y Él te da -- rá,
		to -- "ca y" "te a" -- bri -- rá,
		Je -- su -- cris -- to vi -- ve "y en" -- tre no -- so -- tros es -- tá. __

		Re -- su -- ci -- ta con Él y cre -- e
		es pre -- sen -- te y es ma -- ña -- na
		triun -- fó so -- bre la muer -- te __
		vic -- to -- rio "so u" -- "na e" -- ter -- ni -- dad __
		Es -- "tá en" el en -- fer -- mo que sa -- na
		en el po -- bre que te son -- rí -- e
		en los o -- jos "de un" ni -- ño
		en la tie -- rra "y en" el mar __

		Sien -- te su pre -- sen -- cia
		y a -- dó -- ra -- lo, __
		a -- la -- ba su nom -- bre,
		da -- le glo -- rias al Se -- ñor. __

		Pi -- de y Él te da -- rá,
		to -- "ca y" "te a" -- bri -- rá,
		Je -- su -- cris -- to vi -- ve "y en" -- tre no -- so -- tros es -- tá.

		En el mar de tus pen -- sa -- mien -- tos
		bus -- ca en -- tre tus sen -- ti -- mien -- tos
		u -- "na es" -- pe -- ran -- za, __
		"de un" nue -- "vo en" -- cuen -- tro con Él. __
		Y "no es" -- pe -- res que te sor -- pren -- da
		ya can -- sa -- "do al" fin de la se -- nda
		bus -- ca su ros -- tro __
		y go -- "za u" -- "na e" -- ter -- ni -- dad. __

		Sien -- te su pre -- sen -- cia
		y a -- dó -- ra -- lo, __
		a -- la -- ba su nom -- bre,
		da -- le glo -- rias al Se -- ñor. __

		Pi -- de y Él te da -- rá,
		to -- "ca y" "te a" -- bri -- rá,
		Je -- su -- cris -- to vi -- ve "y en" -- tre no -- so -- tros es -- tá, __
		"y en" -- tre no -- so -- tros es -- tá, __
		"y en" -- tre no -- so -- tros es -- tá. __

	}
>>
